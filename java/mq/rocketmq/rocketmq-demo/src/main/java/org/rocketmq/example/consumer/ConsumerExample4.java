package org.rocketmq.example.consumer;

import lombok.extern.slf4j.Slf4j;
import org.rocketmq.annotation.RocketMQMessageListener;
import org.rocketmq.config.RocketMQListener;
import org.rocketmq.enums.MessageModel;
import org.springframework.stereotype.Component;

/**
 * @Author: zd
 * @Date: 2023-02-06 21:34
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "MyTopicTest", consumerGroup = "ConsumerGroup4",
        selectorExpression = "TAG3",messageModel = MessageModel.BROADCASTING)
public class ConsumerExample4 implements RocketMQListener {

    @Override
    public void onMessage(String message) {
        System.out.println("ConsumerExample4监听到消息==> " + message);
    }
}
