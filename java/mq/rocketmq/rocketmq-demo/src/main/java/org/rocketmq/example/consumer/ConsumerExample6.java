package org.rocketmq.example.consumer;

import lombok.extern.slf4j.Slf4j;
import org.rocketmq.annotation.RocketMQMessageListener;
import org.rocketmq.config.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: zd
 * @Date: 2023-02-06 21:34
 * @Description:
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "MyTopicTest", consumerGroup = "ConsumerGroup6", selectorExpression = "TAG6")
public class ConsumerExample6 implements RocketMQListener {

    @Override
    public void onMessage(String message) {
        System.out.println("ConsumerExample6监听到消息==> " + message);
    }
}
