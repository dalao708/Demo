package org.rocketmq.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zd
 * @date 2023/1/31 11:23
 * @description 属性配置
 */
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQProperties {
    /**
     * rocketmq name server addr
     */
    private String nameServer;

    private Producer producer;

    public String getNameServer() {
        return nameServer;
    }

    public void setNameServer(String nameServer) {
        this.nameServer = nameServer;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }
    public static class Producer {

        /**
         * Group name of producer.
         */
        private String group;

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }
    }
}
