package org.rocketmq.config;

/**
 * @Author: zd
 * @Date: 2023-02-06 10:34
 * @Description: 消费者实现接口
 */
public interface RocketMQListener {

    void onMessage(String message);
}
