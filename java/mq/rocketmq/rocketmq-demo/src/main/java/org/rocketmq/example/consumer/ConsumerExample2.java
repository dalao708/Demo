package org.rocketmq.example.consumer;

import lombok.extern.slf4j.Slf4j;
import org.rocketmq.annotation.RocketMQMessageListener;
import org.rocketmq.config.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Author: zd
 * @Date: 2023-02-06 21:34
 * @Description:
 * 注意：订阅关系一致 <a href="https://rocketmq.apache.org/zh/docs/bestPractice/05subscribe">...</a>
 */
@Slf4j
@Component
@RocketMQMessageListener(topic = "MyTopicTest", consumerGroup = "ConsumerGroup2", selectorExpression = "TAG2")
public class ConsumerExample2 implements RocketMQListener {

    @Override
    public void onMessage(String message) {
        System.out.println("ConsumerExample2监听到消息==> " + message);
    }
}
