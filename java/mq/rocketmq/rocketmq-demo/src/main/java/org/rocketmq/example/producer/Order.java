package org.rocketmq.example.producer;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: zd
 * @Date: 2023-02-08 20:49
 * @Description:
 */
@Data
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单ID
     */
    private Long orderId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 模拟数据，订单创建-支付-完成
     */
    public static List<Order> getOrderList() {
        List<Order> orderList = new ArrayList<>();
        Order orderDemo = new Order();
        orderDemo.setOrderId(1L);
        orderDemo.setOrderStatus("订单创建");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(2L);
        orderDemo.setOrderStatus("订单创建");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(3L);
        orderDemo.setOrderStatus("订单创建");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(4L);
        orderDemo.setOrderStatus("订单创建");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(1L);
        orderDemo.setOrderStatus("订单支付");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(2L);
        orderDemo.setOrderStatus("订单支付");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(1L);
        orderDemo.setOrderStatus("订单完成");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(3L);
        orderDemo.setOrderStatus("订单支付");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(2L);
        orderDemo.setOrderStatus("订单完成");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(3L);
        orderDemo.setOrderStatus("订单完成");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(4L);
        orderDemo.setOrderStatus("订单支付");
        orderList.add(orderDemo);

        orderDemo = new Order();
        orderDemo.setOrderId(4L);
        orderDemo.setOrderStatus("订单完成");
        orderList.add(orderDemo);

        return orderList;

    }
}
