package org.rocketmq.enums;

/**
 * @author zd
 * @date 2023/1/31 12:12
 * @description 消息模式
 */
public enum MessageModel {

    /**
     * 广播模式
     */
    BROADCASTING("BROADCASTING"),
    /**
     * 集群模式、负载均衡模式
     */
    CLUSTERING("CLUSTERING");

    private final String modeCN;

    MessageModel(String modeCN) {
        this.modeCN = modeCN;
    }

    public String getModeCN() {
        return this.modeCN;
    }
}
