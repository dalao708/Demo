package org.rocketmq.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zd
 * @date 2023/1/31 11:39
 * @description 消费者注解
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RocketMQMessageListener {

    String NAME_SERVER_PLACEHOLDER = "${rocketmq.name-server:}";

    /**
     * nameserver addr
     */
    String nameServer() default NAME_SERVER_PLACEHOLDER;

    /**
     * TOPIC
     */
    String topic();

    /**
     * ConsumerGroup消费者分组
     * See <a href="https://rocketmq.apache.org/zh/docs/domainModel/07consumergroup">here</a>
     */
    String consumerGroup();

    /**
     * 过滤类型
     */
    SelectorType selectorType() default SelectorType.TAG;
    /**
     * 过滤表达式，过滤标签Tag
     */
    String selectorExpression() default "*";

    /**
     * Control consume mode, you can choice receive message concurrently or orderly.
     * 消费模式
     */
    ConsumeMode consumeMode() default ConsumeMode.CONCURRENTLY;

    /**
     * 消费者消费消息模式
     * Control message mode, if you want all subscribers receive message all message, broadcasting is a good choice.
     */
    MessageModel messageModel() default MessageModel.CLUSTERING;
}
