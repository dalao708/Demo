package org.rocketmq.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zd
 * @date 2023/1/31 11:23
 * @description 属性配置
 */
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQProperties {
    /**
     * rocketmq name server addr
     */
    private String nameServer;

    public String getNameServer() {
        return nameServer;
    }

    public void setNameServer(String nameServer) {
        this.nameServer = nameServer;
    }
}
