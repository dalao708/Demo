package org.rocketmq.annotation;

/**
 * @author zd
 * @date 2023/1/31 15:06
 * @description 消费模式
 */
public enum ConsumeMode {

    /**
     * Receive asynchronously delivered messages concurrently
     * 并行接收
     */
    CONCURRENTLY,

    /**
     * Receive asynchronously delivered messages orderly. one queue, one thread
     * 顺序接收
     */
    ORDERLY
}
