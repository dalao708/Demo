package org.rocketmq.support;

import org.springframework.beans.factory.DisposableBean;

/**
 * @author zd
 * @date 2023/2/1 9:21
 * @description
 */
public interface RocketMQListenerContainer extends DisposableBean {
}
