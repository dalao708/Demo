package org.rocketmq.core;

/**
 * @author zd
 * @date 2023/2/1 9:15
 * @description The consumer supporting request-reply should implement this interface.
 */
public interface RocketMQReplyListener<R> {

    R onMessage(String message);
}
