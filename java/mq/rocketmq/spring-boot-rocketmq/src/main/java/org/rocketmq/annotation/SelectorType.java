package org.rocketmq.annotation;

/**
 * @author zd
 * @date 2023/1/31 14:06
 * @description 过滤类型
 */
public enum SelectorType {
    /**
     * 标签
     */
    TAG,
    /**
     * SQL属性过滤
     */
    SQL92
}
