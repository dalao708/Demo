package org.rocketmq.core;

/**
 * @author zd
 * @date 2023/1/31 15:14
 * @description 消费者实现接口
 */
public interface RocketMQListener {

    void onMessage(String message);
}
