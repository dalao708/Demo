package strategy;

/**
 * @author zd
 * @date 2022/8/24 17:14
 * @description
 */
public class Test {

    public static void main(String[] args) {
        String exp = "2+8";
        ICalculator cal = new Plus();
        int result = cal.calculate(exp);

        System.out.println(result);
    }
}
