package strategy;

/**
 * @author zd
 * @date 2022/8/24 17:09
 * @description 提供统一的方法
 */
public interface ICalculator {
    public int calculate(String exp);
}
