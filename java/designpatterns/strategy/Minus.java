package strategy;

/**
 * @author zd
 * @date 2022/8/24 17:12
 * @description 实现相减运算
 */
public class Minus extends AbstractCalculator implements ICalculator{
    @Override
    public int calculate(String exp) {
        int[] arrayInt = split(exp,"\\-");
        return arrayInt[0]-arrayInt[1];
    }
}
