package strategy;

/**
 * @author zd
 * @date 2022/8/24 17:13
 * @description 实现相乘运算
 */
public class Multiply extends AbstractCalculator implements ICalculator {

    @Override
    public int calculate(String exp) {
        int[] arrayInt = split(exp,"\\*");
        return arrayInt[0]*arrayInt[1];
    }

}
