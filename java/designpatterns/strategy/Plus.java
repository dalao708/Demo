package strategy;

/**
 * @author zd
 * @date 2022/8/24 17:11
 * @description 实现相加运算
 */
public class Plus extends AbstractCalculator implements ICalculator{
    @Override
    public int calculate(String exp) {
        int[] arrayInt = split(exp,"\\+");
        return arrayInt[0]+arrayInt[1];
    }
}
