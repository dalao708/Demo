package factory_method;

/**
 * @author zd
 * @date 2022/8/23 17:01
 * @description 工厂方法模式，发送邮件和短信的例子
 */
public interface Sender {
    public void send();
}
