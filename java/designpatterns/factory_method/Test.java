package factory_method;

/**
 * @author zd
 * @date 2022/8/23 17:04
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Sender sender = SendFactory.productMail();
        sender.send();

        Sender sender1 = SendFactory.productSms();
        sender1.send();
    }
}
