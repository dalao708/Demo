package factory_method;

/**
 * @author zd
 * @date 2022/8/23 16:58
 * @description 静态工厂方法模式，直接调用
 * 非静态工厂方法模式需要去掉static,需要创建实例才能使用
 */
public class SendFactory {

    public static Sender productMail() {
        return new MailSender();
    }

    public static Sender productSms() {
        return new SmsSender();
    }
}
