package factory_method;

/**
 * @author zd
 * @date 2022/8/23 17:02
 * @description
 */
public class MailSender implements Sender{
    @Override
    public void send() {
        System.out.println("发送邮件");
    }
}
