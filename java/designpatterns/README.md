# java设计模式

#### 创建型模式
- 单例模式 > SingLeton.java
- 工厂方法模式 > factory_method
- 抽象工厂模式 > abstract_factory
- 建造者模式 > builder
- 原型模式 > Prototype

#### 结构型模式
- 适配器模式 > adapter
- 桥接模式 > bridge
- 组合模式 > composite
- 装饰模式 > decorator
- 外观模式 > facade
- 亨元模式 > flyweight
- 代理模式 > proxy

#### 行为型模式
- 访问者模式 > visitor
- 模板方法模式 > template_method
- 策略模式 > strategy
- 状态模式 > state
- 观察者模式 > observer
- 备忘录模式 > memento
- 中介者模式 > mediator
- 迭代器模式 > iterator
- 解释器模式 > interpreter
- 命令模式 > command
- 责任链模式 > chain_of_responsibility

#### 参考博文
1.  [https://blog.csdn.net/sugar_no1/article/details/88317950](https://blog.csdn.net/sugar_no1/article/details/88317950)

