package mediator;

/**
 * @author zd
 * @date 2022/8/27 12:12
 * @description
 */
public class User1 extends User{
    public User1(Mediator mediator){
        super(mediator);
    }

    @Override
    public void work() {
        System.out.println("user1 exe!");
    }

}
