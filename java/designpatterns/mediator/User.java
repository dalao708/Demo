package mediator;

/**
 * @author zd
 * @date 2022/8/27 12:11
 * @description
 */
public abstract class User {

    private Mediator mediator;

    public Mediator getMediator(){
        return mediator;
    }

    public User(Mediator mediator) {
        this.mediator = mediator;
    }

    public abstract void work();
}
