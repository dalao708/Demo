package mediator;

/**
 * @author zd
 * @date 2022/8/27 12:13
 * @description
 */
public class User2 extends User{
    public User2(Mediator mediator){
        super(mediator);
    }

    @Override
    public void work() {
        System.out.println("user2 exe!");
    }

}
