package mediator;

/**
 * @author zd
 * @date 2022/8/27 12:11
 * @description
 */
public interface Mediator {
    public void createMediator();
    public void workAll();
}
