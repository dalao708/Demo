package mediator;

/**
 * @author zd
 * @date 2022/8/27 12:13
 * @description
 */
public class Test {
    public static void main(String[] args) {
        Mediator mediator = new MyMediator();
        mediator.createMediator();
        mediator.workAll();
    }
}
