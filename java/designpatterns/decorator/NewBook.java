package decorator;

/**
 * @author zd
 * @date 2022/8/24 14:41
 * @description
 */
public class NewBook extends Changer{
    public NewBook(IBook iBook) {
        super(iBook);
    }

    public void freeze(){
        System.out.println(iBook.getBook()+"冻结了");
    }

    public void lose(){
        System.out.println(iBook.getBook()+"丢失了");
    }

    @Override
    public void borrowBook() {
        System.out.println(iBook.getBook()+"被NewBook借了");
    }

    @Override
    public void returnBook() {
        System.out.println(iBook.getBook()+"NewBook归还了");
    }
}
