package decorator;

/**
 * @author zd
 * @date 2022/8/24 11:46
 * @description
 */
public class Book extends IBook {
    private String book;

    public Book(String book) {
        this.book = book;
    }

    @Override
    void borrowBook() {
        System.out.println(book + "被Book借了");

    }

    @Override
    void returnBook() {
        System.out.println(book + "Book归还了");
    }

    @Override
    public String getBook() {
        return book;
    }

    @Override
    public void setBook(String book) {
        this.book = book;
    }
}
