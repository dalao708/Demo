package decorator;

/**
 * @author zd
 * @date 2022/8/24 11:42
 * @description 书籍类案例，借书还书
 */
public abstract class IBook {

    private String book;

    abstract void borrowBook();

    abstract void returnBook();

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }
}
