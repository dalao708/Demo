package decorator;

/**
 * @author zd
 * @date 2022/8/24 11:49
 * @description
 */
public class Changer extends IBook{
    private String book;
    public IBook iBook;

    public Changer(IBook iBook) {
        this.iBook = iBook;
    }

    @Override
    void borrowBook() {
        System.out.println(book + "被Changer借了");
    }

    @Override
    void returnBook() {
        System.out.println(book + "Changer归还了");
    }
}
