package decorator;

/**
 * @author zd
 * @date 2022/8/24 14:48
 * @description
 */
public class Test {

    public static void main(String[] args) {
        IBook book = new Book("《设计模式》");
        book.borrowBook();
        book.returnBook();
        NewBook b1=new NewBook(book);
        b1.borrowBook();
        b1.lose();
        b1.freeze();
        b1.returnBook();
    }
}
