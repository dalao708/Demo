package iterator;

/**
 * @author zd
 * @date 2022/8/27 11:13
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Collection collection = new MyCollection();
        Iterator it = collection.iterator();

        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
