package iterator;

/**
 * @author zd
 * @date 2022/8/27 11:03
 * @description
 */
public interface Iterator {
    //前移
    public Object previous();

    //后移
    public Object next();
    public boolean hasNext();

    //取得第一个元素
    public Object first();

}
