package iterator;

/**
 * @author zd
 * @date 2022/8/27 11:09
 * @description
 */
public interface Collection {

    public Iterator iterator();

    /*取得集合元素*/
    public Object get(int i);

    /*取得集合大小*/
    public int size();
}
