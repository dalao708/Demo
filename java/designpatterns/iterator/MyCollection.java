package iterator;

/**
 * @author zd
 * @date 2022/8/27 11:12
 * @description
 */
public class MyCollection implements Collection{

    public String[] string = {"A","B","C","D","E"};
    @Override
    public Iterator iterator() {
        return new MyIterator(this);
    }

    @Override
    public Object get(int i) {
        return string[i];
    }

    @Override
    public int size() {
        return string.length;
    }
}
