package proxy;

/**
 * @author zd
 * @date 2022/8/24 14:54
 * @description
 */
public class Source implements ISource{
    @Override
    public void method() {
        System.out.println("实现方法");
    }
}
