package proxy;

/**
 * @author zd
 * @date 2022/8/24 15:08
 * @description
 */
public class Test {

    public static void main(String[] args) {
        ISource source = new Proxy();
        source.method();
    }
}
