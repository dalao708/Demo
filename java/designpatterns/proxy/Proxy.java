package proxy;

/**
 * @author zd
 * @date 2022/8/24 14:55
 * @description
 */
public class Proxy implements ISource{

    private Source source;
    public Proxy() {
        super();
        this.source = new Source();
    }

    @Override
    public void method() {
        before();
        source.method();
        after();
    }

    private void after() {
        System.out.println("after proxy!");
    }
    private void before() {
        System.out.println("before proxy!");
    }

}
