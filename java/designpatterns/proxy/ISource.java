package proxy;

/**
 * @author zd
 * @date 2022/8/24 14:54
 * @description
 */
public interface ISource {

    void method();
}
