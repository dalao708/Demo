package chain_of_responsibility;

/**
 * @author zd
 * @date 2022/8/27 11:28
 * @description
 */
public abstract class AbstractHandler {

    private Handler handler;

    public Handler getHandler() {
        return handler;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }
}
