package chain_of_responsibility;

/**
 * @author zd
 * @date 2022/8/27 11:27
 * @description
 */
public interface Handler {
    public void operator();
}
