package bridge;

/**
 * @author zd
 * @date 2022/8/24 16:39
 * @description
 */
public class Sub1 implements ISource{
    @Override
    public void method() {
        System.out.println("sub1");
    }
}
