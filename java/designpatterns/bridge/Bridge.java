package bridge;

/**
 * @author zd
 * @date 2022/8/24 16:40
 * @description
 */
public abstract class Bridge {
    private ISource source;

    public void method() {
        source.method();
    }

    public ISource getSource() {
        return source;
    }

    public void setSource(ISource source) {
        this.source = source;
    }
}
