package bridge;

/**
 * @author zd
 * @date 2022/8/24 16:43
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Bridge bridge = new MyBridge();

        ISource source1 = new Sub1();
        bridge.setSource(source1);
        bridge.method();

        ISource source2 = new Sub2();
        bridge.setSource(source2);
        bridge.method();
    }
}
