package bridge;

/**
 * @author zd
 * @date 2022/8/24 16:38
 * @description
 */
public interface ISource {

    void method();
}
