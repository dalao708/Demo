package bridge;

/**
 * @author zd
 * @date 2022/8/24 16:41
 * @description
 */
public class MyBridge extends Bridge {

    @Override
    public void method() {
        getSource().method();
    }
}
