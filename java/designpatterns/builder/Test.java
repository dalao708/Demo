package builder;

/**
 * @author zd
 * @date 2022/8/23 18:23
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Builder builder = new ComputerBuilder();
        // 实际使用可以省去Direcror指挥者角色，直接使用Builder实例来进行对象的组装
        Direcror direcror = new Direcror(builder);
        Computer computer = direcror.createComputer("i7", "inter主板", "三星");
    }
}
