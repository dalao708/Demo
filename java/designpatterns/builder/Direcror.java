package builder;

/**
 * @author zd
 * @date 2022/8/23 18:20
 * @description 指挥者，实现电脑的组装
 */
public class Direcror {
    Builder builder = null;
    public Direcror(Builder builder) {
        this.builder = builder;
    }
    // 通过setter传入
    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public Computer createComputer(String cpu,String mainBoard, String ram) {
        this.builder.buildCpu(cpu);
        this.builder.buildMainBoard(mainBoard);
        this.builder.buildRam(ram);
        return builder.create();
    }
}
