package builder;

/**
 * @author zd
 * @date 2022/8/23 18:07
 * @description 建造者模式，产品类,组装一台电脑
 */
public class Computer {

    private String cpu;
    private String mainBoard;
    private String ram;

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMainBoard() {
        return mainBoard;
    }

    public void setMainBoard(String mainBoard) {
        this.mainBoard = mainBoard;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }
}
