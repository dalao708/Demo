package builder;

/**
 * @author zd
 * @date 2022/8/23 18:14
 * @description 提供安装cpu、主板、和内存的抽象方法，以及组装成电脑的create方法
 */
public abstract class Builder {

    public abstract void buildCpu(String cpu);
    public abstract void buildMainBoard(String mainBoard);
    public abstract void buildRam(String ram);
    public abstract Computer create();

}
