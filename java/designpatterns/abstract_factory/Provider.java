package abstract_factory;

import factory_method.Sender;

/**
 * @author zd
 * @date 2022/8/23 17:08
 * @description 抽象工厂模式，提供接口
 */
public interface Provider {

    public Sender produce();
}
