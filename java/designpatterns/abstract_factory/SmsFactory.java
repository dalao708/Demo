package abstract_factory;

import factory_method.Sender;
import factory_method.SmsSender;

/**
 * @author zd
 * @date 2022/8/23 17:10
 * @description
 */
public class SmsFactory implements Provider{
    @Override
    public Sender produce() {
        return new SmsSender();
    }
}
