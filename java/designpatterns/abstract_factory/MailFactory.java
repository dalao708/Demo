package abstract_factory;

import factory_method.MailSender;
import factory_method.Sender;

/**
 * @author zd
 * @date 2022/8/23 17:10
 * @description
 */
public class MailFactory implements Provider{
    @Override
    public Sender produce() {
        return new MailSender();
    }
}
