package abstract_factory;

/**
 * @author zd
 * @date 2022/8/23 17:06
 * @description 抽象工厂模式
 */
public interface Sender {

    public void send();
}
