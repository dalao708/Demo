package abstract_factory;

/**
 * @author zd
 * @date 2022/8/23 17:03
 * @description
 */
public class SmsSender implements Sender {
    @Override
    public void send() {
        System.out.println("发送短信");
    }
}
