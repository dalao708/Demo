package abstract_factory;

import factory_method.Sender;

/**
 * @author zd
 * @date 2022/8/23 17:11
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Provider provider = new MailFactory();
        Sender produce = provider.produce();
        produce.send();

        Provider provider1 = new SmsFactory();
        Sender produce1 = provider1.produce();
        produce1.send();
    }
}
