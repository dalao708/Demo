package observer;

/**
 * @author zd
 * @date 2022/8/27 10:55
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Subject subject = new MySubject();
        subject.add(new Observer1());
        subject.add(new Observer2());

        subject.operation();
    }
}
