package observer;

/**
 * @author zd
 * @date 2022/8/27 10:44
 * @description
 */
public class Observer1 implements Observer{
    @Override
    public void update() {
        System.out.println("观察者1");
    }
}
