package observer;

/**
 * @author zd
 * @date 2022/8/27 10:43
 * @description
 */
public interface Observer {

    public void update();
}
