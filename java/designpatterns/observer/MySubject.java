package observer;

/**
 * @author zd
 * @date 2022/8/27 10:54
 * @description
 */
public class MySubject extends AbstractSubject{
    @Override
    public void operation() {
        System.err.println("自身操作");
        // 通知观察者
        notifyObservers();
    }
}
