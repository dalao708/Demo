package command;

/**
 * @author zd
 * @date 2022/8/27 11:35
 * @description
 */
public class MyCommand implements Command{
    private Receiver receiver;

    public MyCommand(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void exe() {
        receiver.action();
    }
}
