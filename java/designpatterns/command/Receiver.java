package command;

/**
 * @author zd
 * @date 2022/8/27 11:35
 * @description
 */
public class Receiver {

    public void action(){
        System.out.println("command received!");
    }

}
