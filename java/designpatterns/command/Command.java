package command;

/**
 * @author zd
 * @date 2022/8/27 11:34
 * @description
 */
public interface Command {

    void exe();
}
