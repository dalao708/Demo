package command;

/**
 * @author zd
 * @date 2022/8/27 11:36
 * @description
 */
public class Invoker {

    private Command command;

    public Invoker(Command command) {
        this.command = command;
    }

    public void action(){
        command.exe();
    }
}
