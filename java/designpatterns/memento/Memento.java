package memento;

/**
 * @author zd
 * @date 2022/8/27 11:54
 * @description
 */
public class Memento {

    private String value;

    public Memento(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
