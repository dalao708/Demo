package facade;

/**
 * @author zd
 * @date 2022/8/24 16:33
 * @description
 */
public class Computer {
    private CPU cpu;
    private Disk disk;

    public Computer() {
        cpu = new CPU();
        disk = new Disk();
    }

    public void startup() {
        System.out.println("start the computer!");
        cpu.startup();
        disk.startup();
        System.out.println("start computer finished!");
    }

    public void shutdown() {
        System.out.println("begin to close the computer!");
        cpu.shutdown();
        disk.shutdown();
        System.out.println("computer closed!");
    }
}
