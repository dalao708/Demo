package facade;

/**
 * @author zd
 * @date 2022/8/24 16:32
 * @description
 */
public class CPU {

    public void startup(){
        System.out.println("cpu startup!");
    }

    public void shutdown(){
        System.out.println("cpu shutdown!");
    }
}
