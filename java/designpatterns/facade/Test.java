package facade;

/**
 * @author zd
 * @date 2022/8/24 16:34
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.startup();
        computer.shutdown();
    }
}
