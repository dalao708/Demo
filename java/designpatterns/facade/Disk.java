package facade;

/**
 * @author zd
 * @date 2022/8/24 16:32
 * @description
 */
public class Disk {
    public void startup(){
        System.out.println("disk startup!");
    }

    public void shutdown(){
        System.out.println("disk shutdown!");
    }
}
