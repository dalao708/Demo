package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:12
 * @description
 */
public class Test {

    public void speakTo(Adapter adapter) {
        adapter.speak();
    }

    public static void main(String[] args) {
        Test test = new Test();
        IAnimal dog = new Dog();
        IAnimal cat = new Cat();

        test.speakTo(new Adapter(dog));
        test.speakTo(new Adapter(cat));
    }
}
