package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:06
 * @description 源
 */
public class Cat implements IAnimal{
    @Override
    public void makeSound() {
        System.out.println("猫：喵喵喵。。。");
    }
}
