package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:05
 * @description
 */
public class Dog implements IAnimal{
    @Override
    public void makeSound() {
        System.out.println("狗：汪汪汪。。。");
    }
}
