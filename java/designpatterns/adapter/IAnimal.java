package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:04
 * @description 创建动物发声的例子
 */
public interface IAnimal {

    public void makeSound();
}
