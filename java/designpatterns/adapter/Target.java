package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:09
 * @description 目标角色
 */
public interface Target {

    void speak();
}
