package adapter;

/**
 * @author zd
 * @date 2022/8/24 10:10
 * @description 适配器
 */
public class Adapter implements Target{
    private IAnimal animal;

    public Adapter(IAnimal animal) {
        this.animal = animal;
    }

    @Override
    public void speak() {
        animal.makeSound();
    }
}
