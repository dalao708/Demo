package interpreter;

/**
 * @author zd
 * @date 2022/8/27 12:16
 * @description
 */
public interface Expression {
    public int interpret(Context context);
}
