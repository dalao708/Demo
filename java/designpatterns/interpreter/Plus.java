package interpreter;

/**
 * @author zd
 * @date 2022/8/27 12:16
 * @description
 */
public class Plus implements Expression {
    @Override
    public int interpret(Context context) {
        return context.getNum1() + context.getNum2();
    }
}
