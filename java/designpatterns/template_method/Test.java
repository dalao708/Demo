package template_method;

/**
 * @author zd
 * @date 2022/8/24 17:37
 * @description
 */
public class Test {

    public static void main(String[] args) {
        String exp = "8+8";
        AbstractCalculator cal = new Plus();
        int result = cal.calculate(exp, "\\+");
        System.out.println(result);
    }
}
