package template_method;

/**
 * @author zd
 * @date 2022/8/24 17:37
 * @description
 */
public class Plus extends AbstractCalculator{
    @Override
    public int calculate(int num1, int num2) {
        return num1 + num2;
    }
}
