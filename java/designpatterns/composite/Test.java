package composite;

import sun.reflect.generics.tree.Tree;

/**
 * @author zd
 * @date 2022/8/24 16:51
 * @description
 */
public class Test {

    TreeNode root = null;

    public Test(String name) {
        root = new TreeNode(name);
    }

    public static void main(String[] args) {
        Test tree = new Test("A");
        TreeNode nodeB = new TreeNode("B");
        TreeNode nodeC = new TreeNode("C");

        nodeB.add(nodeC);
        tree.root.add(nodeB);
        System.out.println("build the tree finished!");
        System.out.println(tree.root.getChildren().nextElement().getName());
    }

}
