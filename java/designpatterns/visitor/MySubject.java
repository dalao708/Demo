package visitor;

/**
 * @author zd
 * @date 2022/8/27 12:07
 * @description
 */
public class MySubject implements Subject {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String getSubject() {
        return "访问的属性";
    }
}
