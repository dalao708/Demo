package visitor;

/**
 * @author zd
 * @date 2022/8/27 12:09
 * @description
 */
public class MyVisitor implements Visitor{
    @Override
    public void visit(Subject sub) {
        System.out.println("visit the subject："+sub.getSubject());
    }
}
