package visitor;

/**
 * @author zd
 * @date 2022/8/27 12:08
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Visitor visitor = new MyVisitor();
        Subject sub = new MySubject();
        sub.accept(visitor);
    }
}
