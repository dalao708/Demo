package visitor;

/**
 * @author zd
 * @date 2022/8/27 12:07
 * @description
 */
public interface Visitor {

    public void visit(Subject sub);
}
