package visitor;

/**
 * @author zd
 * @date 2022/8/27 12:06
 * @description 被访问对象，accept方法，接受将要访问它的对象，getSubject()获取将要被访问的属性
 */
public interface Subject {

    public void accept(Visitor visitor);
    public String getSubject();
}
