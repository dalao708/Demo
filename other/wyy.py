# -*- encoding=utf8 -*-
__author__ = "z"
__desc__ = """
            网易云测试
            测试设备：小米6
           """

from airtest.core.api import *
from airtest.report.report import simple_report
from airtest.core.android.android import *
from airtest.core.android.recorder import *
from airtest.core.android.adb import *
from poco.exceptions import PocoNoSuchNodeException
from poco.drivers.android.uiautomation import AndroidUiautomationPoco

poco = AndroidUiautomationPoco()

# 脚本初始化
auto_setup(__file__,devices=["Android://127.0.0.1:5037/192.168.2.101:5555?cap_method=JAVACAP&&ori_method=JAVACAPORI&&touch_method=JAVATOUCH"],logdir=r"E:\work\test\wyy.air\log")

android = Android();

# 进入网易云音乐
def enter_app():
    print("---------------打开网易云音乐---------------")
#     clear_app("com.netease.cloudmusic")
    stop_app("com.netease.cloudmusic")
    start_app("com.netease.cloudmusic")
    sleep(5.0)
    # 判断是否存在搜索栏
    if not poco("com.netease.cloudmusic:id/searchBar").exists():
        print("初次进入网易云")
        # 同意协议
        if poco("android.widget.LinearLayout").exists():
            print("同意协议")
            poco("com.netease.cloudmusic:id/agree").click()
        sleep(1.0)
        while True:
            if poco("miui:id/parentPanel").exists():
                print("同意权限")
                poco("android:id/button1").click()
                sleep(1.0)
            else:
                break;
        # 不登录直接进入页面
        if poco("com.netease.cloudmusic:id/login").exists():
            poco("com.netease.cloudmusic:id/trialT").click()
            poco("com.netease.cloudmusic:id/positiveBtn").click()
            sleep(2.0)
# 操作app
def handle_app():
    print("---------------操作网易云音乐---------------")
#     poco(nameMatches=".*id/desc",text="我的").click()
    poco(nameMatches=".*id/desc",text="发现").click()
    poco("com.netease.cloudmusic:id/searchBar").click()
    sleep(1.0)
    text("周杰伦")
    sleep(1.0)

    poco(text="歌手：周杰伦 (Jay Chou)").wait(2.0).click()
    poco(name="android.widget.TextView",text="歌曲").click()
    sleep(1.0)
    print("播放周杰伦歌曲")
#     poco("com.netease.cloudmusic:id/playText").click()
    
    poco.swipe([0.5,0.5],[0.5,0.1])
    # 保存歌名
    songs = []
    print("开始爬取周杰伦歌曲")
    while True:
        for song in poco(nameMatches = ".*id/musicListItemContainer"):
            a = song.offspring(nameMatches = ".*id/songName")
            b = song.offspring(nameMatches = ".*id/songInfo")
            if not a.exists():
                continue
            name = a.get_text()
            if not name in songs:
                songs.append(name)
                print(name)
                
        # 向上滑动
        poco.swipe([0.5,0.5],[0.5,0.1])        
        sleep(1.0)
        # 找到底部
        if poco("com.netease.cloudmusic:id/viewArtistAllMusic").exists():
            log("总共爬取"+str(len(songs))+"首歌曲的名称")
            print("总共爬取"+str(len(songs))+"首歌曲的名称")
            break
            
try:
    # 开启录屏
    adb = ADB(serialno="192.168.2.101:5555")
    recorder = Recorder(adb)
    recorder.start_recording() 
    
    # 打印序列号
    print("设备号：" + android.get_default_device())
    # 打印第三方应用包名
    print("---------------第三方应用----------------")
    print(android.list_app(third_only=True))

    # 唤醒设备
    if android.is_screenon() == False:
        android.wake()
    # 解锁
    if android.is_locked() == True:
        # 向上滑动解锁
        poco("com.android.systemui:id/keyguard_indication_area").swipe([0,-0.9])
        sleep(1.0)
        # 输入密码解锁
        poco(nameMatches=".*id/key2").click()
        poco(nameMatches=".*id/key5").click()
        poco(nameMatches=".*id/key8").click()
        poco(nameMatches=".*id/key0").click()
        home()
        
    enter_app()
    handle_app()
    
    # 结束录屏
    recorder.stop_recording(output=r"E:\work\test\wyy.air\wyy.mp4")
finally:
    simple_report(__file__,logpath=r"E:\work\test\wyy.air\log",output=r"E:\work\test\wyy.air\log\log.html")
    print("执行结束")
    
